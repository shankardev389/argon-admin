import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ArgonLayoutComponent } from './layouts/argon-layout/argon-layout.component';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  }, {
    path: '',
    component: ArgonLayoutComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('./layouts/argon-layout/argon-layout.module').then(m => m.ArgonLayoutModule)
      }
    ]
  }, {
    path: '**',
    redirectTo: 'dashboard'
  }
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes,{
      useHash: true
    })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
