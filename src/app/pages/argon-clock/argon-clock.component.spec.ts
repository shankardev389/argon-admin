import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArgonClockComponent } from './argon-clock.component';

describe('ArgonClockComponent', () => {
  let component: ArgonClockComponent;
  let fixture: ComponentFixture<ArgonClockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArgonClockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArgonClockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
