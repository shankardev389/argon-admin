import { Component, OnInit, ViewChild, ElementRef, Renderer2, AfterViewInit, Input } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'argon-clock',
  templateUrl: './argon-clock.component.html',
  styleUrls: ['./argon-clock.component.scss']
})
export class ArgonClockComponent implements AfterViewInit {

  @Input() inputTimeZone: string = "";

  months: string[];
  days: string[];

  @ViewChild('date', { static: false }) aDate: ElementRef;
  @ViewChild('time', { static: false }) aTime: ElementRef;
  @ViewChild('day', { static: false }) aDay: ElementRef;
  @ViewChild('spear', { static: false }) aSpear: ElementRef;
  @ViewChild('hour', { static: false }) aHour: ElementRef;
  @ViewChild('minute', { static: false }) aMinute: ElementRef;
  @ViewChild('second', { static: false }) aSecond: ElementRef;
  @ViewChild('dail', { static: false }) aDail: ElementRef;

  constructor(private renderer: Renderer2) {
    this.months = ["January", "February", "March", "April", "May", "June",
      "July", "August", "September", "October", "November", "December"
    ];

    this.days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
  }

  ngAfterViewInit() {
    this.dailer(this.aSecond.nativeElement, 195);
    this.dailer(this.aMinute.nativeElement, 145);
    this.dailer(this.aDail.nativeElement, 230);

    for (var s = 1; s < 13; s++) {
      const span: ElementRef = this.renderer.createElement('span');
      this.renderer.setStyle(span, "transform", "rotate(" + 30 * s + "deg) translateX(100px)");
      const text = this.renderer.createText(s.toString());
      this.renderer.appendChild(span,text);

      this.renderer.appendChild(this.aHour.nativeElement, span);
    }


    setInterval(() => { this.getTime() }, 1000);
    this.getTime();
  }

  getTime() {

    console.log(this.inputTimeZone);

    var date: Date = new Date();
    var second = date.getSeconds();
    var minute = date.getMinutes();
    var hour = date.getHours();
    var time = date.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true, timeZone: this.inputTimeZone });
    var day = date.getDay();
    var month = date.getMonth();
    var vDate = date.getDate() + ' . ' + this.months[month];

    var ds = second * -6;
    var dm = minute * -6;
    var dh = hour * -30;

    this.renderer.setStyle(this.aSecond.nativeElement, 'transform', 'rotate(' + ds + 'deg)');
    this.renderer.setStyle(this.aMinute.nativeElement, 'transform', 'rotate(' + dm + 'deg)');
    this.renderer.setStyle(this.aHour.nativeElement, 'transform', 'rotate(' + dh + 'deg)');

    this.aTime.nativeElement.innerHTML = time;
    this.aDay.nativeElement.innerHTML = this.days[day];
    this.aDate.nativeElement.innerHTML = vDate;
  }

  dailer(selector, size) {
    console.log(selector);

    for (var s = 0; s < 60; s++) {
      const span: ElementRef = this.renderer.createElement('span');
      this.renderer.setStyle(span, "transform", "rotate(" + 6 * s + "deg) translateX(" + size + "px)");
      const text = this.renderer.createText(s.toString());
      this.renderer.appendChild(span,text);

      this.renderer.appendChild(selector, span);

    }
  }

}
