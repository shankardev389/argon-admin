import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConvertClockComponent } from './convert-clock.component';

describe('ConvertClockComponent', () => {
  let component: ConvertClockComponent;
  let fixture: ComponentFixture<ConvertClockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConvertClockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConvertClockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
