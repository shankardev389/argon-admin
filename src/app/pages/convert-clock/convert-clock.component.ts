import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-convert-clock',
  templateUrl: './convert-clock.component.html',
  styleUrls: ['./convert-clock.component.scss']
})
export class ConvertClockComponent implements OnInit {

  timeZones: string[] = ["America/New_York","Asia/Kolkata"];

  constructor() { }

  ngOnInit() {
  }

}
