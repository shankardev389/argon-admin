import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArgonLayoutComponent } from './argon-layout.component';

describe('ArgonLayoutComponent', () => {
  let component: ArgonLayoutComponent;
  let fixture: ComponentFixture<ArgonLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArgonLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArgonLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
