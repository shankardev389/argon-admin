import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from 'src/app/pages/dashboard/dashboard.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ClipboardModule } from 'ngx-clipboard';
import { RouterModule } from '@angular/router';
import { ArgonLayoutRoutes } from '../argon-layout-routing';
import { ArgonClockComponent } from 'src/app/pages/argon-clock/argon-clock.component';
import { ConvertClockComponent } from 'src/app/pages/convert-clock/convert-clock.component';

@NgModule({
  declarations: [
    DashboardComponent,
    ArgonClockComponent,
    ConvertClockComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    NgbModule,
    ClipboardModule,
    RouterModule.forChild(ArgonLayoutRoutes)
  ]
})
export class ArgonLayoutModule { }
