import { Routes } from '@angular/router';
import { DashboardComponent } from '../pages/dashboard/dashboard.component';
import { ArgonClockComponent } from '../pages/argon-clock/argon-clock.component';
import { ConvertClockComponent } from '../pages/convert-clock/convert-clock.component';

export const ArgonLayoutRoutes: Routes = [
    {
        path: 'dashboard', 
        component: DashboardComponent
    },
    {
        path: 'convert-clock',
        component: ConvertClockComponent
    }
]